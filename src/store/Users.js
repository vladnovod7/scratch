import { types, flow } from 'mobx-state-tree';
import API              from '../api';

const api = new API();

const User = types.model({
    address      : '',
    dob          : '',
    email        : '',
    'first_name' : '',
    gender       : '',
    id           : '',
    'last_name'  : '',
    phone        : '',
    status       : '',
    website      : '',
    disabled     : types.optional(types.boolean, false)
}).views(self => ({
    get fullName() {
        return `${self.first_name} ${self.last_name}`;
    },
    get isActive() {
        return self.status === 'active';
    }
})).actions(self => ({
    toggleDisabled() {
        self.disabled = !self.disabled;
    }
}));

export default types.model({
    list         : types.array(User),
    usersLoading : types.optional(types.boolean, false)
})
    .actions(self => ({
        fetchUsers : flow(function* fetchUsers(query) {
            self.usersLoading = true;
            try {
                const list = yield api.get('users', query);

                self.usersLoading = false;
                self.list = list;
            } catch (error) {
                console.error('Failed to fetch projects', error);
                self.state = 'error';
            }
        })
    }));
