import { types, flow } from 'mobx-state-tree';
import API              from '../api';

const api = new API();

const Post =  types.model({
    id        : '',
    body      : '',
    title     : '',
    'user_id' : types.string
});

export default types.model({
    list         : types.array(Post),
    postsLoading : types.optional(types.boolean, false)
})
    .actions(self => ({
        fetchPosts : flow(function* fetchPosts() {
            self.postsLoading = true;
            try {
                self.list = yield api.get('posts');
                self.postsLoading = false;
            } catch (error) {
                console.error('Failed to fetch projects', error);
                self.state = 'error';
            }
        })

    }));
