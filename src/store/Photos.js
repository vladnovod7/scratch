import { types, flow } from 'mobx-state-tree';
import API              from '../api';

const api = new API();

const Photo =  types.model({
    id        : '',
    album_id  : '236',
    title     : '',
    url       : '',
    thumbnail : ''
});

export default types.model({
    list          : types.array(Photo),
    photosLoading : types.optional(types.boolean, false)
})
    .actions(self => ({
        fetchPhotos : flow(function* fetchPhotos() {
            self.photosLoading = true;
            try {
                self.list = yield api.get('photos');
                self.photosLoading = false;
            } catch (error) {
                console.error('Failed to fetch projects', error);
                self.state = 'error';
            }
        })

    }));
