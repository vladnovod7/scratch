import { types, getSnapshot } from 'mobx-state-tree';
import Users             from './Users';
import Posts             from './Posts';
import Photos            from './Photos';

const RootStore = types.model({
    users  : types.optional(Users, {}),
    posts  : types.optional(Posts, {}),
    photos : types.optional(Photos, {})
});

const store = RootStore.create();

console.log(getSnapshot(store));

export default store;
