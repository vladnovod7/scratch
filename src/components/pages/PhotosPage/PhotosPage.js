import React from 'react';
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';
import styled from 'styled-components';
import AnimatedList from '../../ui-kit/AnimatedList';

const PageWrapper = styled.div`
    padding: 10px;
`;

const StyledImageWrapper = styled.div`
    margin: 10px;
`;

const GridWrapper = styled.div`
    display: flex;
`;

const ColumnWrapper = styled.div`
    width: calc(100% / 4);
`;

const StyledImage = styled.img`
    width: 100%;
`;

@inject('store')
@observer
class PhotosPage extends React.PureComponent {
    static propTypes = {
        store : PropTypes.shape({
            photos : PropTypes.object.isRequired
        }).isRequired
    }

    componentDidMount = () => {
        this.props.store.photos.fetchPhotos();
    }


    splitArrayByColumns = (array, count) => {
        const result = [];

        array.forEach((item, index) => {
            const splitIndex = index % count;

            if (!result[splitIndex]) {
                result[splitIndex] = [];
            }

            result[splitIndex].push(item);
        });

        return result;
    }

    renderColumn = (images) => {
        return (
            <ColumnWrapper>
                <AnimatedList wrapper={StyledImageWrapper}>
                    {images.map(item => <StyledImage src={item.url} key={item.id} />)}
                </AnimatedList>
            </ColumnWrapper>
        );
    }

    render() {
        const { photos } = this.props.store;
        const { list } = photos;

        const data = this.splitArrayByColumns(list, 4);

        console.log(list);

        return (
            <PageWrapper>
                <GridWrapper>
                    {data.map(imagesList => this.renderColumn(imagesList))}
                </GridWrapper>
            </PageWrapper>
        );
    }
}

export default PhotosPage;
