import React from 'react';
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';
import styled from 'styled-components';

import Pagination from '../../ui-kit/Pagination';
import AnimatedList from '../../ui-kit/AnimatedList';
import UserComponent from '../../ui-kit/UserComponent';

const PageWrapper = styled.div`
    padding: 10px;
`;

const StyledUserComponentWrapper = styled.div`
 &:not(:last-child) {
    margin-bottom: 10px;
 }
`;


@inject('store')
@observer
class UsersPage extends React.Component {
    static propTypes = {
        store : PropTypes.shape({
            users : PropTypes.array.isRequired
        }).isRequired
    }

    componentDidMount = () => {
        this.props.store.users.fetchUsers();
    }

    handlePageChange = (page) => {
        this.props.store.users.fetchUsers({ page });
    }

    render() {
        const { users } = this.props.store;
        const { usersLoading, list } = users;

        console.log(list);

        return (
            <>
                <PageWrapper>
                    <Pagination
                        max={60} min={1} current={1}
                        onClick={this.handlePageChange}
                    />
                    <AnimatedList wrapper={StyledUserComponentWrapper} isLoading={usersLoading}>
                        {list.map(user => <UserComponent key={user.id} user={user} />)}
                    </AnimatedList>
                </PageWrapper>
            </>
        );
    }
}

export default UsersPage;
