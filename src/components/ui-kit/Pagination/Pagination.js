import React from 'react';
import styled from 'styled-components';

const PaginationWrapper = styled.div`
    display: flex;
    justify-content: flex-start;
    margin-bottom: 10px;
    
`;

const Button = styled.div`
    width: 40px;
    height: 40px;
    line-height: 40px;
    text-align: center;
    border-radius: 4px;
    box-shadow: 6px 6px 5px 0px rgba(0,0,0,0.26);
    transition: 0.2s;
    background: ${props => props.theme.main.dark}
    display: ${props => !props.isNotVisible ? 'block' : 'none' };
    color : ${props => props.active && props.theme.main.primary};
    transform: ${props => props.active ? 'scale(1.05)' : ''};

    &:not(:last-child) {
        margin-right: 5px;
    }

    &:hover {
        cursor: pointer;
        transform: scale(1.05);
        background: ${props => props.theme.main.secondary}
    }
`;

class Pagination extends React.Component {
    state = {
        current : 1
    }

    handleClick = (e) => {
        this.setState({ current: parseInt(e.target.innerHTML, 10) });

        this.props.onClick(parseInt(e.target.innerHTML, 10));
    }

    render() {
        const { max, min } = this.props;
        const { current } = this.state;

        const minVisible = current - 1 <= min;
        const prevVisible = current === min;
        const maxVisible = current + 1 >= max;
        const nextVisible = current === max;

        return (
            <PaginationWrapper>
                <Button isNotVisible={minVisible} onClick={this.handleClick} >{min}</Button>
                <Button isNotVisible={minVisible}>...</Button>
                <Button isNotVisible={prevVisible} onClick={this.handleClick}>{current - 1}</Button>
                <Button active onClick={this.handleClick}>{current}</Button>
                <Button isNotVisible={nextVisible} onClick={this.handleClick}>{current + 1}</Button>
                <Button isNotVisible={maxVisible} >...</Button>
                <Button isNotVisible={maxVisible} onClick={this.handleClick}>{max}</Button>
            </PaginationWrapper>
        );
    }
}

export default Pagination;
