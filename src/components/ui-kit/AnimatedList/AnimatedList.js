import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Wrapper = styled.div`
    opacity: 0;
    transform: translateY(-300px);
    animation: ${props => !props.isLoading ? 'fadeIn 0.4s forwards' : 'fadeIn 0s forwards'}; 
    animation-delay: ${props => !props.isLoading ? (props.index + 1) * 0.05 : '0'}s;
    position: relative;

    @keyframes fadeIn {
  0% {
    opacity: 0;
    transform: translateY(-100px);
  }
  60% {
    transform: translateY(10px);
  }
  100% {
    opacity: 1;
    transform: translateY(0px);
  }
}
`;

const LoadingWrapper = styled.div`
  -webkit-backface-visibility: hidden;
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  opacity: 0.6;
  z-index: 21;
  display: flex;
}
`;

const LoadingLayer = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    z-index: 22;
    background: rgba(255,255,255,0.1);
`;

class AnimatedList extends React.Component {
    static propTypes = {
        wrapper   : PropTypes.node.isRequired,
        children  : PropTypes.node.isRequired,
        isLoading : PropTypes.bool
    }

    static defaultProps = {
        isLoading : false
    }

    render() {
        const { isLoading } = this.props;
        const StyledWrapper = this.props.wrapper || React.Fragment;

        return (
            this.props.children.map((component, index) => {
                return (
                    <StyledWrapper key={component.key}>
                        <Wrapper className={component.props.className} index={index} isLoading={isLoading}>
                            {component}
                            {isLoading && <LoadingLayer />}
                        </Wrapper>
                    </StyledWrapper>
                );
            })
        );
    }
}

export default AnimatedList;
